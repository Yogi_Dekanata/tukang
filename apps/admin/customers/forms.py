from django import forms

from apps.users_authentication.models import User
from .models import Customer

class SearchForm(forms.Form):
    q = forms.CharField()


class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())

    class Meta:
        model = User
        fields = ('username', 'password')
        exclude = ['username']


class CustomerForm(forms.ModelForm):
    first_name = forms.CharField(max_length=100)
    last_name = forms.CharField(max_length=100)
    phone_number = forms.CharField(max_length=20)
    email = forms.EmailField()

    # password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = Customer
        fields = ['first_name', 'last_name', 'phone_number', 'email', ]

    def clean_email(self):
        email = self.cleaned_data['email']

        if Customer.objects.filter(email=email).count() > 1:
            raise forms.ValidationError("This email  already exist.")
        return email

    def clean_phone_number(self):
        phone_number = self.cleaned_data['phone_number']

        if Customer.objects.filter(phone_number=phone_number).count() > 1:
            raise forms.ValidationError("This phone_number already exist.")
        return phone_number

        # class DeliveryManForm(forms.ModelForm):
        #     # def __init__(self, *args, **kwargs):
        #     #     super(DeliveryManForm, self).__init__(*args, **kwargs)
        #     #     self.fields['email'].queryset = User.objects.using('customer')
        #     # self.fields['first_name'].queryset = User.objects.using('customer')
        #     # self.fields['photo'].queryset = User.objects.using('customer')
        #     # self.fields['phone_number'].queryset = User.objects.using('customer')
        #     # self.fields['password'].queryset = User.objects.using('customer')
        #
        #     first_name = forms.CharField(max_length=100)
        #     last_name = forms.CharField(max_length=100)
        #     photo = forms.ImageField()
        #     phone_number = forms.CharField(max_length=20)
        #     email = forms.EmailField()
        #     password = forms.CharField(widget=forms.PasswordInput)
        #
        #     #
        #     # def is_valid(self):
        #     #     valid = super(DeliveryManForm, self).is_valid()
        #     #     print valid
        #     #     # we're done now if not     valid
        #     #     if not valid:
        #     #         print 'wkwkwkw'
        #     #         return valid
        #
        #     class Meta:
        #         model = DeliveryMan
        #         fields = ['first_name', 'last_name', 'phone_number', 'email', 'password', 'photo']
        #
        #         def clean_email(self):
        #             email = self.cleaned_data['email']
        #             if User.objects.using('customer').filter(email=email).count() > 1:
        #                 raise forms.ValidationError("This email  already exist.")
        #             return email
        #
        #         def clean_phone_number(self):
        #             phone_number = self.cleaned_data['phone_number']
        #             if User.objects.using('customer').filter(phone_number=phone_number).count() > 1:
        #                 print 'hallo'
        #                 raise forms.ValidationError("This phone_number already exist.")
        #             return phone_number



        # def save(self, commit=True):
        #     print 'wowowow'
        #     product_to_save = super(DeliveryManForm, self).save(commit=False)
        #         # the above creates product_to_save instance, but doesn't saves it to DB
        #     product_to_save.save(using = 'customer')
        #     return product_to_save





        # def clean_email(self):
        #     email = self.cleaned_data['email']
        #
        #     if User.objects.using('customer').filter(email=email).exists():
        #         raise forms.ValidationError("This email  already exist.")
        #     return email
        #
        # def clean_phone_number(self):
        #     phone_number = self.cleaned_data['phone_number']
        #     if User.objects.using('customer').filter(phone_number=phone_number).exists():
        #         raise forms.ValidationError("This phone_number already exist.")
        #     return phone_number


class DeliveryManFormEdit(forms.ModelForm):
    first_name = forms.CharField(max_length=100)
    last_name = forms.CharField(max_length=100)
    photo = forms.ImageField()
    phone_number = forms.CharField(max_length=20)
    email = forms.EmailField()
    password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = User

        fields = ['first_name', 'last_name', 'phone_number', 'email', 'password', 'photo']

    def clean_email(self):
        email = self.cleaned_data['email']
        if User.objects.using('customer').filter(email=email).count() > 1:
            raise forms.ValidationError("This email  already exist.")
        return email

    def clean_phone_number(self):
        phone_number = self.cleaned_data['phone_number']
        if User.objects.using('customer').filter(phone_number=phone_number).count() > 1:
            print 'hallo'
            raise forms.ValidationError("This phone_number already exist.")
        return phone_number






        #
        # def save(self, *args, **kwargs):
        #     Staff = super(CustomerForm, self).save(commit=False)
        #     user = User.objects.create(
        #         first_name=self.cleaned_data['first_name'],
        #         last_name=self.cleaned_data['last_name'],
        #         phone_number = '085784962863',
        #         email=self.cleaned_data['email'])
        #     user.set_password(self.cleaned_data['password'])
        #
        #     Staff.user = user
        #     Staff.save()
        #

#
#
# class AuthorForm(ModelForm):
#     class Meta:
#         model = User
#         fields = ['first_name', 'last_name', 'photo', 'phone_number', 'password']
