from django.contrib.auth.models import (
    AbstractBaseUser, PermissionsMixin
)
from django.contrib.auth.models import User
# from django.contrib.auth.models import User
# from apps.users_authentication.models import User
from django.core.validators import RegexValidator
from django.db import models
from django.db.models import Q
from django.db.models.signals import post_delete
from django.dispatch import receiver
# from apps.users_authentication.models import User, MyUserManager
from rest_framework.authtoken.models import Token

from apps.users_authentication.models import User


class SearchManager(models.Manager):
    def search(self, **kwargs):
        qs = self.get_queryset()
        if kwargs.get('q', ''):
            qs = qs.filter(
                Q(first_name__icontains=kwargs['q']) | Q(last_name__icontains=kwargs['q']) | Q(
                    email__icontains=kwargs['q']) | Q(
                    phone_number__icontains=kwargs['q']))
        return qs


class Customer(models.Model):
    user = models.OneToOneField(User, related_name='customer', null=True, on_delete=models.SET_NULL)
    email = models.EmailField(
        verbose_name='email address',
        max_length=255,
        unique=True,
    )

    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,13}$',
                                 message="Phone number must be entered in the format: '081xxxxxxx/021xxxxxxx'. Up to 15 digits allowed.")

    phone_number = models.CharField(validators=[phone_regex], unique=True, max_length=20)  # validators should be a list
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)
    is_customer = models.BooleanField(default=True)

    objects = SearchManager()

    def __unicode__(self):
        return '%s  %s' % (self.first_name, self.last_name)


@receiver(post_delete, sender=Customer)
def auto_delete_publish_info_with_book(sender, instance, **kwargs):
    instance.user.delete()
