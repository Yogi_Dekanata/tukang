from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^customers/', views.customer, name='customers'),
    url(r'^add-customer/', views.add_customer, name='add_customer'),
    url(r'^delete-customer/', views.delete_customer, name='delete_customer'),
    url(r'^edit-customer/(?P<id>\d+)/$', views.edit_customer, name='edit_customer'),
    url(r'^search-customer/$', views.search_customer, name='search_customer'),

]
