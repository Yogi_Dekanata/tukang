# Create your views here.
from django.contrib.auth.decorators import user_passes_test
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.urlresolvers import reverse
from django.shortcuts import render, redirect, get_object_or_404
from rest_framework.authtoken.models import Token

from apps.users_authentication.models import User
from .forms import CustomerForm, UserForm, SearchForm
from .models import Customer


@user_passes_test(lambda u: u.is_superuser)
def customer(request):
    users = Customer.objects.all()

    paginator = Paginator(users, 20)

    page = request.GET.get('page')
    try:
        users = paginator.page(page)
    except PageNotAnInteger:
        users = paginator.page(1)
    except EmptyPage:
        users = paginator.page(paginator.num_pages)
    search_form = SearchForm()

    return render(request, 'admin/customers/list.html', {'users': users, 'search_form': search_form})


@user_passes_test(lambda u: u.is_superuser)
def add_customer(request):
    if request.method == 'POST':
        uniqe_id = User.objects.last().id
        customer_form = CustomerForm(data=request.POST)
        user_form = UserForm(data=request.POST)

        if customer_form.is_valid() and user_form.is_valid():
            cd = customer_form.cleaned_data
            user = user_form.save(commit=False)
            user.username = '%s %s ' % (cd['email'], uniqe_id)
            user.set_password(user.password)
            user.save()
            token, created = Token.objects.get_or_create(user=user)
            customer = customer_form.save(commit=False)
            customer.user = user
            customer.save()
            return redirect(reverse('admin_customers:customers'))
    else:
        customer_form = CustomerForm()
        user_form = UserForm()

    return render(request, 'admin/customers/add.html', {'customer_form': customer_form,
                                                        'user_form': user_form})


@user_passes_test(lambda u: u.is_superuser)
def delete_customer(request):
    if request.method == 'POST':
        print 'ada apa'
        delete_selected = request.POST.getlist('delete_selected')
        Customer.objects.filter(id__in=delete_selected).delete()
    return redirect(reverse('admin_customers:customers'))


@user_passes_test(lambda u: u.is_superuser)
def edit_customer(request, id):
    instance_customer = get_object_or_404(Customer, id=id)
    instance_user = get_object_or_404(User, id=instance_customer.user.id)

    if request.method == 'POST':
        customer_form = CustomerForm(data=request.POST, instance=instance_customer)
        user_form = UserForm(data=request.POST, instance=instance_user)
        if customer_form.is_valid():
            user = user_form.save(commit=False)
            user.set_password(user.password)
            user.save()
            customer = customer_form.save(commit=False)
            customer.user = user
            customer.save()
            return redirect(reverse('admin_customers:customers'))
    else:
        customer_form = CustomerForm(instance=instance_customer)
        user_form = UserForm(instance=instance_user)

    return render(request, 'admin/customers/edit.html', {'customer_form': customer_form,
                                                         'user_id': id,
                                                         'user_form': user_form})


@user_passes_test(lambda u: u.is_superuser)
def search_customer(request):
    search_form = SearchForm(request.GET)
    if search_form.is_valid():
        users = Customer.objects.search(**search_form.cleaned_data)
    else:
        users = Customer.objects.all()
    paginator = Paginator(users, 25)

    page = request.GET.get('page')
    try:
        users = paginator.page(page)
    except PageNotAnInteger:
        users = paginator.page(1)
    except EmptyPage:
        users = paginator.page(paginator.num_pages)
    search_form = SearchForm()

    return render(request, 'admin/customers/list.html', {'users': users, 'search_form': search_form})
