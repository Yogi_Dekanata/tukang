from django import forms

from apps.users_authentication.models import User
from .models import DeliveryMan


class SearchForm(forms.Form):
    q = forms.CharField()


class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())

    class Meta:
        model = User
        fields = ('username', 'password')
        exclude = ['username']


class CustomerForm(forms.ModelForm):
    first_name = forms.CharField(max_length=100)
    last_name = forms.CharField(max_length=100)
    phone_number = forms.CharField(max_length=20)
    email = forms.EmailField()
    password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'phone_number', 'email', 'password']

    def clean_email(self):
        email = self.cleaned_data['email']

        if User.objects.filter(email=email).count() > 1:
            raise forms.ValidationError("This email  already exist.")
        return email

    def clean_phone_number(self):
        phone_number = self.cleaned_data['phone_number']

        if User.objects.filter(phone_number=phone_number).count() > 1:
            raise forms.ValidationError("This phone_number already exist.")
        return phone_number


class DeliveryManForm(forms.ModelForm):
    first_name = forms.CharField(max_length=100)
    last_name = forms.CharField(max_length=100)
    photo = forms.ImageField()
    phone_number = forms.CharField(max_length=20)
    email = forms.EmailField()
    password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = DeliveryMan
        fields = ['first_name', 'last_name', 'phone_number', 'email', 'password', 'photo']

        def clean_email(self):
            email = self.cleaned_data['email']
            if User.objects.using('customer').filter(email=email).count() > 1:
                raise forms.ValidationError("This email  already exist.")
            return email

        def clean_phone_number(self):
            phone_number = self.cleaned_data['phone_number']
            if User.objects.using('customer').filter(phone_number=phone_number).count() > 1:
                print 'hallo'
                raise forms.ValidationError("This phone_number already exist.")
            return phone_number


class DeliveryManFormEdit(forms.ModelForm):
    first_name = forms.CharField(max_length=100)
    last_name = forms.CharField(max_length=100)
    photo = forms.ImageField()
    phone_number = forms.CharField(max_length=20)
    email = forms.EmailField()
    password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = User

        fields = ['first_name', 'last_name', 'phone_number', 'email', 'password', 'photo']

    def clean_email(self):
        email = self.cleaned_data['email']
        if User.objects.using('customer').filter(email=email).count() > 1:
            raise forms.ValidationError("This email  already exist.")
        return email

    def clean_phone_number(self):
        phone_number = self.cleaned_data['phone_number']
        if User.objects.using('customer').filter(phone_number=phone_number).count() > 1:
            print 'hallo'
            raise forms.ValidationError("This phone_number already exist.")
        return phone_number
