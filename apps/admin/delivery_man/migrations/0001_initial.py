# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
from django.conf import settings
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='DeliveryMan',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('is_delivery_man', models.BooleanField(default=True)),
                ('email', models.EmailField(unique=True, max_length=255, verbose_name=b'email address', error_messages={b'unique': b'This email has already been registered.'})),
                ('phone_number', models.CharField(unique=True, max_length=20, validators=[django.core.validators.RegexValidator(regex=b'^\\+?1?\\d{9,13}$', message=b"Phone number must be entered in the format: '081xxxxxxx/021xxxxxxx'. Up to 15 digits allowed.")])),
                ('is_active', models.BooleanField(default=True)),
                ('is_admin', models.BooleanField(default=False)),
                ('is_staff', models.BooleanField(default=False)),
                ('first_name', models.CharField(max_length=150)),
                ('last_name', models.CharField(max_length=150)),
                ('photo', models.ImageField(upload_to=b'')),
                ('user', models.OneToOneField(related_name='delivery_man', null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
