from django.contrib.auth.models import (
    AbstractBaseUser, PermissionsMixin
)
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
# from django.contrib.auth.models import User
# from apps.users_authentication.models import User
from django.core.validators import RegexValidator
from django.db import models
# from apps.users_authentication.models import User, MyUserManager
from django.db.models import Q
from rest_framework.authtoken.models import Token

from apps.users_authentication.models import User


class SearchManager(models.Manager):
    def search(self, **kwargs):
        qs = self.get_queryset()
        if kwargs.get('q', ''):
            qs = qs.filter(
                Q(first_name__icontains=kwargs['q']) | Q(last_name__icontains=kwargs['q']) | Q(
                    email__icontains=kwargs['q']) | Q(
                    phone_number__icontains=kwargs['q']))
        return qs


class DeliveryMan(models.Model):
    is_delivery_man = models.BooleanField(default=True)
    email = models.EmailField(
        verbose_name='email address',
        max_length=255,
        unique=True,
        error_messages={'unique': "This email has already been registered."}
    )
    user = models.OneToOneField(User, related_name='delivery_man', null=True, on_delete=models.SET_NULL)
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,13}$',
                                 message="Phone number must be entered in the format: '081xxxxxxx/021xxxxxxx'. Up to 15 digits allowed.")

    phone_number = models.CharField(validators=[phone_regex], unique=True, max_length=20)  # validators should be a list

    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)
    photo = models.ImageField()

    objects = SearchManager()

    def __unicode__(self):
        return '%s' % self.email

    def get_absolute_url(self):
        return reverse('admin_delivery_man:edit_delivery_man', kwargs={'id': self.id})
