from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^delivery-man/', views.delivery_man, name='delivery_man'),
    url(r'^add-delivery-man/', views.add_delivery_man, name='add_delivery_man'),
    url(r'^delete-delivery-man/', views.delete_delivery_man, name='delete_delivery_man'),
    url(r'^edit-delivery-man/(?P<id>\d+)/$', views.edit_delivery_man, name='edit_delivery_man'),
    url(r'^search-delivery-man/$', views.search_delivery_man, name='search_delivery_man'),

]
