# Create your views here.
from django.contrib.auth.decorators import user_passes_test
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.urlresolvers import reverse
from django.shortcuts import render, redirect, get_object_or_404
from rest_framework.authtoken.models import Token

from apps.users_authentication.models import User
from .forms import DeliveryManForm, UserForm, SearchForm
from .models import DeliveryMan


@user_passes_test(lambda u: u.is_superuser)
def delivery_man(request):
    users = DeliveryMan.objects.all()

    paginator = Paginator(users, 5)  # Show 25 contacts per page

    page = request.GET.get('page')
    try:
        users = paginator.page(page)
    except PageNotAnInteger:
        users = paginator.page(1)
    except EmptyPage:
        users = paginator.page(paginator.num_pages)
    search_form = SearchForm()

    return render(request, 'admin/deleveryman/list.html', {'users': users, 'search_form': search_form})


@user_passes_test(lambda u: u.is_superuser)
def add_delivery_man(request):
    if request.method == 'POST':
        uniqe_id = User.objects.last().id
        delivery_man_form = DeliveryManForm(request.POST, request.FILES)
        user_form = UserForm(data=request.POST)
        if delivery_man_form.is_valid() and user_form.is_valid():
            cd = delivery_man_form.cleaned_data
            user = user_form.save(commit=False)
            user.username = '%s %s ' % (cd['email'], uniqe_id)
            user.set_password(user.password)
            user.save()
            token, created = Token.objects.get_or_create(user=user)
            customer = delivery_man_form.save(commit=False)
            customer.user = user
            customer.save()

            return redirect(reverse('admin_delivery_man:delivery_man'))
    else:

        delivery_man_form = DeliveryManForm()
        user_form = UserForm()

    return render(request, 'admin/deleveryman/add.html', {'user_form': user_form,
                                                          'delivery_man_form': delivery_man_form})


@user_passes_test(lambda u: u.is_superuser)
def delete_delivery_man(request):
    if request.method == 'POST':
        delete_selected = request.POST.getlist('delete_selected')
        DeliveryMan.objects.filter(id__in=delete_selected).delete()
    return redirect(reverse('admin_delivery_man:delivery_man'))


@user_passes_test(lambda u: u.is_superuser)
def edit_delivery_man(request, id):
    instance_delivery_man = get_object_or_404(DeliveryMan, id=id)
    instance_user = get_object_or_404(User, id=instance_delivery_man.user.id)

    if request.method == 'POST':
        # increment total image views by 1
        delivery_man_form = DeliveryManForm(request.POST, request.FILES, instance=instance_delivery_man)
        user_form = UserForm(data=request.POST, instance=instance_user)
        if delivery_man_form.is_valid():
            cd = delivery_man_form.cleaned_data
            user = user_form.save(commit=False)
            user.set_password(user.password)
            user.save()
            customer = delivery_man_form.save(commit=False)
            customer.user = user
            customer.save()
            return redirect(reverse('admin_delivery_man:delivery_man'))
    else:
        delivery_man_form = DeliveryManForm(instance=instance_delivery_man)
        user_form = UserForm(instance=instance_user)

    return render(request, 'admin/deleveryman/edit.html', {'delivery_man_form': delivery_man_form,
                                                           'user_form': user_form})


@user_passes_test(lambda u: u.is_superuser)
def search_delivery_man(request):
    search_form = SearchForm(request.GET)
    if search_form.is_valid():
        users = DeliveryMan.objects.search(**search_form.cleaned_data)
    else:
        users = DeliveryMan.objects.all()
    paginator = Paginator(users, 5)  # Show 25 contacts per page

    page = request.GET.get('page')
    try:
        users = paginator.page(page)
    except PageNotAnInteger:
        users = paginator.page(1)
    except EmptyPage:
        users = paginator.page(paginator.num_pages)
    search_form = SearchForm()

    return render(request, 'admin/deleveryman/list.html', {'users': users, 'search_form': search_form})
