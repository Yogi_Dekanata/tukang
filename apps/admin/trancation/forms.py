from django import forms

from apps.admin.customers.models import Customer
from apps.admin.users_management.models import City
from .models import ProductPrice, Order, ProductOrder, AdminCustom

valid_time_formats = ['%H:%M %p']
APPROVAL_CHOICES = (
    (u'Awaiting', u'Awaiting'),
    (u'Progress', u'Progress'),
    (u'Done', u'Done'),
)


class SearchForm(forms.Form):
    q = forms.CharField()


class ProductPriceForm(forms.ModelForm):
    brand = forms.CharField()
    image = forms.ImageField()
    price = forms.DecimalField()

    class Meta:
        model = ProductPrice
        fields = ('brand', 'image', 'price',)

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super(ProductPriceForm, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = self.cleaned_data
        admin = AdminCustom.objects.get(id=self.request.user.user_admin.id)
        existing = ProductPrice.objects.filter(brand=cleaned_data['brand'], admin=admin
                                               ).count()
        if existing > 0:
            raise forms.ValidationError({'brand': 'Brand musbe uniqe'})


class ProductPriceSuperAdminForm(forms.ModelForm):
    brand = forms.CharField()
    image = forms.ImageField()
    price = forms.DecimalField()

    class Meta:
        model = ProductPrice
        fields = ('brand', 'image', 'price',)
        exclude = ('admin',)

    def clean(self):
        cleaned_data = self.cleaned_data
        city = self.data.get('city_name')
        print self.data
        admin = AdminCustom.objects.get(city__id=city)
        existing = ProductPrice.objects.filter(brand=cleaned_data['brand'], admin=admin
                                               ).count()
        if existing > 0:
            raise forms.ValidationError({'brand': 'Brand muse be uniqe'})


class ProductOrderForm(forms.ModelForm):
    def __init__(self, user, city, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super(ProductOrderForm, self).__init__(*args, **kwargs)
        self.initial['brand'] = 'default value'

        admin = AdminCustom.objects.get(city__name_city=city)
        try:
            brand_select = forms.ChoiceField(
                choices=([(i.brand, i.brand) for i in ProductPrice.objects.filter(admin_id=user.user_admin.id)]))
        except Exception:
            brand_select = forms.ChoiceField(
                choices=([(i.brand, i.brand) for i in ProductPrice.objects.filter(admin_id=admin)]))

        self.fields['brand'] = brand_select

    class Meta:
        model = ProductOrder
        fields = ('brand', 'price', 'quantity')
        exclude = ('product_order',)


class CitySuperAdminForm(forms.Form):
    city_name = forms.ModelChoiceField(queryset=City.objects.all(), )

    class Meta:
        model = City
        fields = ('city_name',)


class CityForm(forms.ModelForm):
    city_name = forms.ModelChoiceField(queryset=City.objects.all(), )

    class Meta:
        model = City
        fields = ('city_name',)


class OrderCustomerForm(forms.ModelForm):
    user_order = forms.ModelChoiceField(queryset=Customer.objects.all())  # Or whatever query you'd like
    time = forms.TimeField(input_formats=valid_time_formats)
    status = forms.ChoiceField(choices=APPROVAL_CHOICES)

    class Meta:
        model = Order
        fields = '__all__'
        exclude = ['time']
        widgets = {
            'user_order': forms.Select(attrs={'class': 'select'}),
        }


class OrderCustomerSuperAdminForm(forms.ModelForm):
    user_order = forms.ModelChoiceField(queryset=Customer.objects.all())  # Or whatever query you'd like
    time = forms.TimeField(input_formats=valid_time_formats)
    status = forms.ChoiceField(choices=APPROVAL_CHOICES)

    class Meta:
        model = Order
        fields = '__all__'
        exclude = ['time']
        widgets = {
            'user_order': forms.Select(attrs={'class': 'select'}),
        }
