# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users_management', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProductPrice',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('brand', models.CharField(max_length=200, null=True, blank=True)),
                ('price', models.DecimalField(max_digits=6, decimal_places=2)),
                ('image', models.ImageField(null=True, upload_to=b'', blank=True)),
                ('admin', models.ForeignKey(related_name='product_price', blank=True, to='users_management.AdminCustom', null=True)),
            ],
        ),
    ]
