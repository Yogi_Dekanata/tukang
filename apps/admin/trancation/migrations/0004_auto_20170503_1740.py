# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('trancation', '0003_order'),
    ]

    operations = [
        migrations.RenameField(
            model_name='order',
            old_name='location',
            new_name='longitude',
        ),
        migrations.AddField(
            model_name='order',
            name='latitude',
            field=models.DecimalField(null=True, max_digits=9, decimal_places=6, blank=True),
        ),
    ]
