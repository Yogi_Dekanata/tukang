# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('trancation', '0005_auto_20170503_1754'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='product_order',
            field=models.ForeignKey(related_name='product_order', blank=True, to='trancation.ProductPrice', null=True),
        ),
    ]
