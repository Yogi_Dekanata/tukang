# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('trancation', '0006_auto_20170503_1915'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='order',
            name='product_order',
        ),
        migrations.AddField(
            model_name='productorder',
            name='product_order',
            field=models.ForeignKey(related_name='product_order', blank=True, to='trancation.Order', null=True),
        ),
    ]
