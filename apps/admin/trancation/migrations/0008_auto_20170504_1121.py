# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('trancation', '0007_auto_20170504_1057'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='phone_number',
            field=models.CharField(max_length=20, validators=[django.core.validators.RegexValidator(regex=b'^\\+?1?\\d{9,13}$', message=b"Phone number must be entered in the format: '081xxxxxxx/021xxxxxxx'. Up to 15 digits allowed.")]),
        ),
    ]
