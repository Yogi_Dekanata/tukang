# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('trancation', '0008_auto_20170504_1121'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='delivery_date',
            field=models.DateField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='order',
            name='time',
            field=models.TimeField(null=True, blank=True),
        ),
    ]
