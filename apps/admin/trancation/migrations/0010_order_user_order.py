# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0001_initial'),
        ('trancation', '0009_auto_20170504_1606'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='user_order',
            field=models.ForeignKey(related_name='user_order', blank=True, to='customers.Customer', null=True),
        ),
    ]
