# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('trancation', '0011_auto_20170504_2313'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='status',
            field=models.CharField(default=1, max_length=1, choices=[('0', 'Awaiting'), ('1', 'Progress'), ('2', 'Done')]),
            preserve_default=False,
        ),
    ]
