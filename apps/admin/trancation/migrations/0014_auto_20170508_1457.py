# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('trancation', '0013_auto_20170508_1202'),
    ]

    operations = [
        migrations.AddField(
            model_name='productorder',
            name='quantity',
            field=models.IntegerField(default=1),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='order',
            name='status',
            field=models.CharField(max_length=100, choices=[('Awaiting', 'Awaiting'), ('Progress', 'Progress'), ('Done', 'Done')]),
        ),
    ]
