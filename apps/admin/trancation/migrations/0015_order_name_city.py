# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users_management', '0003_auto_20170507_0025'),
        ('trancation', '0014_auto_20170508_1457'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='name_city',
            field=models.ForeignKey(verbose_name=b'name_city', blank=True, to='users_management.City', null=True),
        ),
    ]
