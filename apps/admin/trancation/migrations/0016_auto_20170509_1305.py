# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('trancation', '0015_order_name_city'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='productprice',
            unique_together=set([('brand', 'admin')]),
        ),
    ]
