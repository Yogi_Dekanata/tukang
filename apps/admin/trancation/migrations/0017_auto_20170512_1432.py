# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('trancation', '0016_auto_20170509_1305'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='status',
            field=models.CharField(default=b'Awaiting', max_length=100, choices=[('Awaiting', 'Awaiting'), ('Progress', 'Progress'), ('Done', 'Done')]),
        ),
    ]
