from django.core.validators import RegexValidator
from django.db import models
from django.db.models import Q
from django_extensions.db.models import (
    TitleSlugDescriptionModel, TimeStampedModel)

from apps.admin.customers.models import Customer
from apps.admin.users_management.models import AdminCustom, City

APPROVAL_CHOICES = (
    (u'Awaiting', u'Awaiting'),
    (u'Progress', u'Progress'),
    (u'Done', u'Done'),
)


class SearchManagerProduct(models.Manager):
    def search(self, **kwargs):
        qs = self.get_queryset()
        if kwargs.get('q', ''):
            qs = qs.filter(
                Q(brand__icontains=kwargs['q']) | Q(price__icontains=kwargs['q']) | Q(
                    admin__city__name_city=kwargs['q']))
        return qs


class SearchManagerOder(models.Manager):
    def search(self, **kwargs):
        qs = self.get_queryset()
        if kwargs.get('q', ''):
            qs = qs.filter(
                Q(user_order__user__username=kwargs['q']) | Q(phone_number__icontains=kwargs['q']))
        return qs


class ProductPrice(models.Model):
    brand = models.CharField(max_length=200, blank=True, null=True)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    image = models.ImageField(null=True, blank=True)
    admin = models.ForeignKey(AdminCustom, related_name='product_price', blank=True, null=True)

    objects = SearchManagerProduct()

    class Meta:
        unique_together = ('brand', 'admin',)

    def __unicode__(self):
        return '%s' % self.brand


class Order(TimeStampedModel):
    user_order = models.ForeignKey(Customer, related_name='user_order', blank=True, null=True)
    longitude = models.DecimalField(max_digits=9, decimal_places=6, blank=True, null=True)
    latitude = models.DecimalField(max_digits=9, decimal_places=6, blank=True, null=True)
    detail_location = models.CharField(max_length=150, blank=True, null=True)
    name_user = models.CharField(max_length=50, blank=True, null=True)
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,13}$',
                                 message="Phone number must be entered in the format: '081xxxxxxx/021xxxxxxx'. Up to 15 digits allowed.")

    phone_number = models.CharField(validators=[phone_regex], max_length=20)
    delivery_date = models.DateField(blank=True, null=True)
    time = models.TimeField(blank=True, null=True)
    name_city = models.ForeignKey(City, blank=True, null=True, verbose_name='name_city')
    status = models.CharField(max_length=100, choices=APPROVAL_CHOICES, default='Awaiting')

    objects = SearchManagerOder()

    def __unicode__(self):
        return '%s' % self.name_user


class ProductOrder(models.Model):
    brand = models.CharField(max_length=200, blank=True, null=True)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    quantity = models.IntegerField()
    product_order = models.ForeignKey(Order, related_name='product_order', blank=True, null=True)

    def __unicode__(self):
        return '%s' % self.brand
