from django.conf.urls import url

from apps.admin.trancation import views

urlpatterns = [
    url(r'^product-price/', views.product_price, name='product_price'),
    url(r'^add-product-price/', views.add_product_price, name='add_product_price'),
    url(r'^add-product-price-admin/', views.add_product_price_super_admin, name='add_product_price_super_admin'),
    url(r'^edit-product-price/(?P<id>\d+)/$', views.edit_product_price, name='edit_product_price'),
    url(r'^delete-product-price/', views.delete_product_price, name='delete_product_price'),
    url(r'^search-product-price/', views.search_product_price, name='search_product_price'),

    url(r'^order/', views.order, name='order'),
    url(r'^add-order/', views.add_order, name='add_order'),
    url(r'^add-order-admin/', views.add_order_super_admin, name='add_order_super_admin'),
    url(r'^order-detail/(?P<id>\d+)/$', views.order_show, name='order_show'),
    url(r'^delete-order/', views.delete_order, name='delete_order'),
    url(r'^search-order/$', views.search_order, name='search_order'),

    url(r'^add-order-product/(?P<id>\d+)/$', views.add_order_product, name='add_order_product'),
    url(r'^edit-order-product/(?P<id>\d+)/$', views.edit_order_product, name='edit_order_product'),
    url(r'^select-brand-product/$', views.select_brand_product, name='select_brand_product'),
    url(r'^delete-order-product/$', views.delete_order_product, name='delete_order_product'),

]
