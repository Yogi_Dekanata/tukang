from django.contrib.auth.decorators import login_required, user_passes_test
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.urlresolvers import reverse
from django.http import JsonResponse
from django.shortcuts import render, redirect, get_object_or_404

from .forms import ProductPriceForm, OrderCustomerForm, ProductOrderForm, ProductPriceSuperAdminForm, \
    CitySuperAdminForm, OrderCustomerSuperAdminForm, SearchForm
from .models import ProductPrice, Order, City, AdminCustom, ProductOrder


# Create your views here.
@login_required
def product_price(request):
    try:
        admin_id = request.user.user_admin.id
    except Exception:
        products = ProductPrice.objects.all()
    else:
        products = ProductPrice.objects.filter(admin_id=admin_id)

    paginator = Paginator(products, 5)  # Show 25 contacts per page

    page = request.GET.get('page')
    try:
        products = paginator.page(page)
    except PageNotAnInteger:
        products = paginator.page(1)
    except EmptyPage:
        products = paginator.page(paginator.num_pages)
    search_form = SearchForm()

    return render(request, 'admin/trancation/product_price/list.html',
                  context={'products': products, 'search_form': search_form})


@login_required
def add_product_price(request):
    if request.method == 'POST':
        product_price_form = ProductPriceForm(request.POST, request.FILES, request=request)
        if product_price_form.is_valid():
            product_price = product_price_form.save(commit=False)
            product_price.admin = request.user.user_admin
            product_price.save()
            return redirect(reverse('admin_trancation:product_price'))
    else:

        product_price_form = ProductPriceForm()
    return render(request, 'admin/trancation/product_price/add.html',
                  context={'product_price_form': product_price_form})


@user_passes_test(lambda u: u.is_superuser)
def add_product_price_super_admin(request):
    if request.method == 'POST':
        product_price_form = ProductPriceSuperAdminForm(request.POST, request.FILES)
        city_form = CitySuperAdminForm(request.POST)
        if product_price_form.is_valid() and city_form.is_valid():
            product_price = product_price_form.save(commit=False)
            city = city_form.cleaned_data['city_name']
            admin = AdminCustom.objects.get(city__name_city=city)
            product_price.admin = admin
            product_price.save()
            return redirect(reverse('admin_trancation:product_price'))
    else:

        product_price_form = ProductPriceForm()
        city_form = CitySuperAdminForm()
    return render(request, 'admin/trancation/product_price/add.html',
                  context={'product_price_form': product_price_form, 'city_form': city_form})


@login_required
def delete_product_price(request):
    if request.method == 'POST':
        delete_selected = request.POST.getlist('delete_selected')
        ProductPrice.objects.filter(id__in=delete_selected).delete()
    return redirect(reverse('admin_trancation:product_price'))


@login_required
def edit_product_price(request, id):
    instance_product_price = get_object_or_404(ProductPrice, id=id)
    if request.method == 'POST':
        product_price_form = ProductPriceForm(request.POST, request.FILES, instance=instance_product_price)
        if product_price_form.is_valid():
            customer = product_price_form.save(commit=False)
            customer.save()
            return redirect(reverse('admin_trancation:product_price'))
    else:
        product_price_form = ProductPriceForm(instance=instance_product_price)

    return render(request, 'admin/trancation/product_price/edit.html',
                  context={'product_price_form': product_price_form,
                           })


@login_required
def order(request):
    try:
        name_city = request.user.user_admin.city
    except Exception:
        orders = Order.objects.all()
    else:
        orders = Order.objects.filter(name_city=name_city)

    paginator = Paginator(orders, 5)  # Show 25 contacts per page

    page = request.GET.get('page')
    try:
        orders = paginator.page(page)
    except PageNotAnInteger:
        orders = paginator.page(1)
    except EmptyPage:
        orders = paginator.page(paginator.num_pages)
    search_form = SearchForm()

    return render(request, 'admin/trancation/order/list.html', context={'orders': orders, 'search_form': search_form})


@login_required
def add_order(request):
    if request.method == 'POST':
        order_customer_form = OrderCustomerForm(request.POST)
        city = request.user.user_admin.city.id
        if order_customer_form.is_valid():  # and city_form.is_valid():
            instance_order_customer = order_customer_form.save(commit=False)
            city = City.objects.get(id=city)
            instance_order_customer.name_city = city
            instance_order_customer.save()
            return redirect('admin_trancation:add_order_product', id=instance_order_customer.id)
    else:

        order_customer_form = OrderCustomerForm()

    return render(request, 'admin/trancation/order/add.html',
                  context={'order_customer_form': order_customer_form, }
                  )


@user_passes_test(lambda u: u.is_superuser)
def add_order_super_admin(request):
    if request.method == 'POST':
        order_customer_form = OrderCustomerSuperAdminForm(request.POST)
        city_form = CitySuperAdminForm(request.POST)
        if order_customer_form.is_valid() and city_form.is_valid():  # and city_form.is_valid():
            instance_order_customer = order_customer_form.save(commit=False)
            city = city_form.cleaned_data['city_name']
            city = City.objects.get(name_city=city)
            instance_order_customer.name_city = city
            instance_order_customer.save()
            return redirect('admin_trancation:add_order_product', id=instance_order_customer.id)
    else:

        order_customer_form = OrderCustomerForm()
        city_form = CitySuperAdminForm(request.POST)

    return render(request, 'admin/trancation/order/add.html',
                  context={'order_customer_form': order_customer_form,
                           'city_form': city_form}
                  )


@login_required
def order_show(request, id):
    order = get_object_or_404(Order, id=id)
    total = sum([i.price * i.quantity for i in order.product_order.all()])
    return render(request, 'admin/trancation/order/detail.html', context={'order': order, 'total': total})


@login_required
def delete_order(request):
    if request.method == 'POST':
        delete_selected = request.POST.getlist('delete_selected')
        Order.objects.filter(id__in=delete_selected).delete()
    return redirect(reverse('admin_trancation:order'))


@login_required
def add_order_product(request, id):
    order = get_object_or_404(Order, id=id)
    city = order.name_city
    if request.method == 'POST':
        order_product_customer_from = ProductOrderForm(request.user, city, request.POST)
        if order_product_customer_from.is_valid():
            instance_order_product_customer = order_product_customer_from.save(commit=False)
            instance_order_product_customer.product_order = order
            instance_order_product_customer.save()
            return redirect('admin_trancation:order')
    else:

        order_product_customer_from = ProductOrderForm(request.user, city)
    return render(request, 'admin/trancation/order/product/add_order_product.html',
                  context={'order_product_customer': order_product_customer_from, 'order_id': order.id})


@login_required
def edit_order_product(request, id):
    instance_order_product = ProductOrder.objects.get(id=id)

    if request.method == 'POST':
        order_product_customer_from = ProductOrderForm(request.user, request.POST, instance=instance_order_product)
        if order_product_customer_from.is_valid():
            order_product_customer_from.save(commit=False)
            order_product_customer_from.product_order = instance_order_product
            order_product_customer_from.save()
            return redirect('admin_trancation:order')
    else:
        order_product_customer_from = ProductOrderForm(request.user, instance=instance_order_product)
        return render(request, 'admin/trancation/order/product/add_order_product.html',
                      context={'order_product_customer': order_product_customer_from})


@login_required
def delete_order_product(request):
    if request.method == 'POST':
        delete_selected = request.POST.getlist('delete_selected')
        ProductOrder.objects.filter(id__in=delete_selected).delete()
    return redirect(reverse('admin_trancation:order'))


@login_required
def select_brand_product(request):
    if request.is_ajax():
        brand = request.POST.get('brand')
        id_order_product = request.POST.get('id_order_product')
        order = get_object_or_404(Order, id=id_order_product)
        try:
            admin = AdminCustom.objects.get(id=request.user.user_admin.id)
        except Exception:
            admin = AdminCustom.objects.get(city__name_city=order.name_city)
            product = ProductPrice.objects.filter(brand=brand, admin=admin)
        else:
            product = ProductPrice.objects.filter(brand=brand, admin=admin)
        product_price = product.values_list('price', flat=True)
        return JsonResponse({'brand_price': product_price[0]})


@user_passes_test(lambda u: u.is_superuser)
def search_product_price(request):
    search_form = SearchForm(request.GET)
    if search_form.is_valid():
        products = ProductPrice.objects.search(**search_form.cleaned_data)
    else:
        products = ProductPrice.objects.all()
    paginator = Paginator(products, 15)  # Show 25 contacts per page

    page = request.GET.get('page')
    try:
        products = paginator.page(page)
    except PageNotAnInteger:
        products = paginator.page(1)
    except EmptyPage:
        products = paginator.page(paginator.num_pages)
    search_form = SearchForm()

    return render(request, 'admin/trancation/product_price/list.html',
                  context={'products': products, 'search_form': search_form})


@user_passes_test(lambda u: u.is_superuser)
def search_order(request):
    search_form = SearchForm(request.GET)
    if search_form.is_valid():
        orders = Order.objects.search(**search_form.cleaned_data)
    else:
        orders = Order.objects.all()
    paginator = Paginator(orders, 15)  # Show 25 contacts per page

    page = request.GET.get('page')
    try:
        orders = paginator.page(page)
    except PageNotAnInteger:
        orders = paginator.page(1)
    except EmptyPage:
        orders = paginator.page(paginator.num_pages)
    search_form = SearchForm()

    return render(request, 'admin/trancation/order/list.html',
                  context={'orders': orders, 'search_form': search_form})
