from django import forms

from apps.users_authentication.models import User
from .models import City, AdminCustom

class SearchForm(forms.Form):
    q = forms.CharField()


class TestTableModelChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        # return the field you want to display
        return obj.name_city


class UserForm(forms.ModelForm):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput())

    class Meta:
        model = User
        fields = ('username', 'password')
        # exclude = ['username']


class CityForm(forms.ModelForm):
    name_city = forms.CharField()

    class Meta:
        model = City
        fields = ('name_city',)


class CityListForm(forms.ModelForm):
    name_city = TestTableModelChoiceField(queryset=City.objects.all(),)

    class Meta:
        model = City
        fields = ('name_city',)


class CitiesForm(forms.Form):
    name_city = TestTableModelChoiceField(queryset=City.objects.all(),)


# class AdminCustomForm(fo)



