# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='AdminCustom',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('is_admin_custom', models.BooleanField(default=True)),
            ],
        ),
        migrations.CreateModel(
            name='City',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name_city', models.CharField(max_length=150, unique=True, null=True, verbose_name=b'Name City', blank=True)),
            ],
        ),
        migrations.AddField(
            model_name='admincustom',
            name='city',
            field=models.ForeignKey(related_name='city_admin', blank=True, to='users_management.City', null=True),
        ),
        migrations.AddField(
            model_name='admincustom',
            name='user',
            field=models.OneToOneField(related_name='user_admin', null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL),
        ),
    ]
