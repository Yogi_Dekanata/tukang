from django.core.urlresolvers import reverse
from django.db import models
from django.db.models import Q
from django.db.models.signals import post_delete
from django.dispatch import receiver

from apps.users_authentication.models import User


class SearchManager(models.Manager):
    def search(self, **kwargs):
        qs = self.get_queryset()
        if kwargs.get('q', ''):
            qs = qs.filter(
                Q(city__name_city__icontains=kwargs['q']) | Q(user__username__icontains=kwargs['q']))
        return qs


class City(models.Model):
    name_city = models.CharField(max_length=150, unique=True, blank=True, null=True, verbose_name='Name City')

    def __unicode__(self):
        return '%s' % self.name_city


class AdminCustom(models.Model):
    user = models.OneToOneField(User, related_name='user_admin', null=True, on_delete=models.SET_NULL)
    is_admin_custom = models.BooleanField(default=True)
    city = models.ForeignKey(City, related_name='city_admin', blank=True, null=True)

    objects = SearchManager()

    def __unicode__(self):
        return '%s' % self.user

    def get_name_city(self):
        return '%s' % self.city.name_city

    def get_absolute_url(self):
        return reverse('admin_users_management:edit_users_management', kwargs={'id': self.user_id})


@receiver(post_delete, sender=AdminCustom)
def auto_delete_user(sender, instance, **kwargs):
    instance.user.delete()
