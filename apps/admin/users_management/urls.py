from django.conf.urls import url
from django.contrib.auth.decorators import login_required

from apps.admin.users_management import views

urlpatterns = [
    url(r'^users-management/', views.users_management, name='users_management'),
    url(r'^add-users-management/', views.add_user_management, name='add_users_management'),

    # url(r'^city-autocomplete/$', views.CityAutocomplete.as_view(), name='city-autocomplete'),
    url(r'^city/', login_required(views.CityView.as_view()), name='add_city'),
    url(r'^city-delete/', views.delete_city, name='delete-city'),

    url(r'^delete-users-management/', views.delete_users_management, name='delete_users_management'),
    url(r'^edit-users-management/(?P<id>\d+)/$', views.edit_users_management, name='edit_users_management'),
    url(r'^search-users-management/$', views.search_users_management, name='search_users_management'),
]
