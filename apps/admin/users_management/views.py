from django.contrib.auth.decorators import user_passes_test
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.urlresolvers import reverse
from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import View

from apps.users_authentication.models import User
from .forms import UserForm, CityForm, CityListForm, CitiesForm, SearchForm
from .models import City, AdminCustom


class CityView(View):
    form_class = CityForm
    template_name = 'admin/users_management/cities/list.html'

    def get(self, request, *args, **kwargs):
        form = self.form_class()
        cities = City.objects.all()
        return render(request, self.template_name, {'city_form': form, 'cities': cities, })

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            form.save()
            return redirect('admin_users_management:add_users_management')
        cities = City.objects.all()

        return render(request, self.template_name, {'city_form': form, 'cities': cities, })


def delete_city(request):
    if request.method == 'POST':
        delete_selected = request.POST.getlist('delete_selected')
        City.objects.filter(id__in=delete_selected).delete()
    return redirect(reverse('admin_users_management:add_city'))


@user_passes_test(lambda u: u.is_superuser)
def users_management(request):
    admins = AdminCustom.objects.all().select_related('user', 'city')
    # admins = AdminCustom.objects.all()

    paginator = Paginator(admins, 3)  # Show 25 contacts per page

    page = request.GET.get('page')
    try:
        admins = paginator.page(page)
    except PageNotAnInteger:
        admins = paginator.page(1)
    except EmptyPage:
        admins = paginator.page(paginator.num_pages)
    search_form = SearchForm()
    return render(request, 'admin/users_management/list.html', {'admins': admins, 'search_form': search_form})


@user_passes_test(lambda u: u.is_superuser)
def add_user_management(request):
    if request.method == 'POST':
        uniqe_id = User.objects.last().id
        user_form = UserForm(request.POST)
        city_list_form = CityListForm(request.POST)
        city_form = CityForm(request.POST)

        if user_form.is_valid() and city_form.is_valid():
            new_user = User.objects.create_user(**user_form.cleaned_data)
            city = City.objects.get(id=request.POST.get('name_city'))
            new_user.save()
            add_admin = AdminCustom()
            add_admin.user = new_user
            add_admin.city = city
            add_admin.save()

            return redirect(reverse('admin_users_management:users_management'))
    else:

        user_form = UserForm()
        city_form = CityForm()
        # cities = City.objects.all()
        city_list_form = CityListForm()
    return render(request, 'admin/users_management/add.html',
                  {'user_form': user_form,
                           # 'cities': cities,
                           'city_form': city_form,
                           'city_list_form': city_list_form})


@user_passes_test(lambda u: u.is_superuser)
def delete_users_management(request):
    if request.method == 'POST':
        delete_selected = request.POST.getlist('delete_selected')
        AdminCustom.objects.filter(id__in=delete_selected).delete()
    return redirect(reverse('admin_users_management:users_management'))


@user_passes_test(lambda u: u.is_superuser)
def edit_users_management(request, id):
    instance_user = get_object_or_404(User, id=id)

    if request.method == 'POST':
        user_form = UserForm(data=request.POST, instance=instance_user)
        city_list_form = CitiesForm(request.POST)

        if user_form.is_valid() and city_list_form.is_valid():
            user = user_form.save(commit=False)
            name_city = city_list_form.cleaned_data['name_city']

            city = City.objects.get(name_city=name_city)
            admin_custom = AdminCustom.objects.get(user_id=user.id)
            admin_custom.city = city
            admin_custom.save()
            user.set_password(user.password)
            user.save()

            return redirect(reverse('admin_users_management:users_management'))
    else:
        city_list_form = CitiesForm()

        user_form = UserForm(instance=instance_user)

    return render(request, 'admin/users_management/edit.html',
                  {'city_list_form': city_list_form,
                           'user_id': id,
                           'user_form': user_form})


@user_passes_test(lambda u: u.is_superuser)
def search_users_management(request):
    search_form = SearchForm(request.GET)
    if search_form.is_valid():
        admins = AdminCustom.objects.search(**search_form.cleaned_data)
    else:
        admins = AdminCustom.objects.all()
    paginator = Paginator(admins, 25)  # Show 25 contacts per page

    page = request.GET.get('page')
    try:
        admins = paginator.page(page)
    except PageNotAnInteger:
        admins = paginator.page(1)
    except EmptyPage:
        admins = paginator.page(paginator.num_pages)
    search_form = SearchForm()

    return render(request, 'admin/users_management/list.html', {'admins': admins, 'search_form': search_form})
