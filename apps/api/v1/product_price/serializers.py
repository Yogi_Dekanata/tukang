from django.contrib.auth import get_user_model  # If used custom user model
from rest_framework import serializers
from apps.admin.trancation.models import ProductPrice



class ProductPriceSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductPrice
        fields = ('brand', 'price', 'image', )
