from django.conf.urls import url

from apps.api.v1.product_price import views

urlpatterns = [
    url(r'^product-price/$', views.ProductPriceList.as_view(), name='api_product_price'),
]

