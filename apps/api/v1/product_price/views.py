from rest_framework.response import Response
from rest_framework.views import APIView

from apps.admin.trancation.models import ProductPrice
from .serializers import ProductPriceSerializer


class ProductPriceList(APIView):
    """
    List all snippets, or create a new snippet.
    """

    def get(self, request, format=None):
        snippets = ProductPrice.objects.all()
        serializer = ProductPriceSerializer(snippets, many=True)
        return Response(serializer.data)
