from rest_framework import serializers

from apps.admin.trancation.models import Order, ProductOrder


class ProductOrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductOrder
        fields = ('brand', 'price','quantity')


class OrderSerializer(serializers.ModelSerializer):
    product_order = ProductOrderSerializer(many=True)

    class Meta:
        model = Order
        fields = (
            'product_order', 'longitude', 'latitude', 'detail_location', 'name_user', 'phone_number', 'product_order',)

    def create(self, validated_data):
        product_order_data = validated_data.pop('product_order')
        product_order = Order.objects.create(**validated_data)
        for track_data in product_order_data:
            ProductOrder.objects.create(product_order=product_order, **track_data)
        return product_order
