from django.conf.urls import url

from apps.api.v1.transaction import views

urlpatterns = [
    url(r'^order/$', views.Order.as_view(), name='order'),
]

