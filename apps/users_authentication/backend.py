# from apps.api.v1.users.models import Customer
# from apps.users_authentication.models import User
from .models import User


class AuthenticationBackend(object):
    def authenticate(self, username=None, password=None, **kwargs):
        try:

            user = User.objects.get(username=username)
            if user.check_password(password) and user.is_active():
                return user
        except User.DoesNotExist:

            User().set_password(password)
