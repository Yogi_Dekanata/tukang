from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser, PermissionsMixin
)
# from django.contrib.auth.models import User
from django.db import models
from django_extensions.db.models import (TitleSlugDescriptionModel, TimeStampedModel)
from rest_framework.authtoken.models import Token


class MyUserManager(BaseUserManager):
    def create_user(self, password=None, username=None, **extra_fields):
        """
        Creates and saves a User with the given email, date of
        birth and password.
        """

        user = self.model(
            username=username, **extra_fields
        )

        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, password, **extra_fields):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """
        user = self.create_user(
            username=username,
            password=password,

        )
        user.is_admin = True
        user.save(using=self._db)
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        return user


class User(AbstractBaseUser):
    username = models.CharField(unique=True, max_length=30)

    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)

    objects = MyUserManager()

    USERNAME_FIELD = 'username'

    def get_full_name(self):
        return self.username

    def get_short_name(self):
        return self.username

    def __unicode__(self):  # __unicode__ on Python 2
        return unicode(self.username)

    @property
    def is_superuser(self):
        return self.is_admin

    @property
    def is_staff(self):
        return self.is_admin

    def has_perm(self, perm, obj=None):
        return self.is_admin

    def has_module_perms(self, app_label):
        return self.is_admin
