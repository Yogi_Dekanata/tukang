from django.contrib.auth import get_user_model  # If used custom user model
from rest_framework import serializers
from apps.admin.customers.models import Customer

UserModel = get_user_model()


class CustomerSerializer(serializers.ModelSerializer):
    password = serializers.CharField(source='user.password')

    class Meta:
        model = Customer
        fields = ('first_name', 'last_name', 'phone_number', 'email', 'password',)

    def create(self, validated_data):
        user_data = validated_data.pop('user')

        uniqe_id = UserModel.objects.last().id
        user = UserModel.objects.create(
            username='%s %s' % (validated_data['email'], uniqe_id)
        )
        user.set_password(user_data['password'])
        user.save()

        customer = Customer.objects.create(user=user, **validated_data)

        return customer.user
