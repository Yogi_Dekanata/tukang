import datetime

from django.conf import settings
from django.contrib.auth import authenticate, login
from django.shortcuts import render, redirect
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.admin.customers.models import Customer
from  serializers import CustomerSerializer
from .forms import LoginForm


class RegistrationCustomerView(APIView):
    permission_classes = (AllowAny,)

    def post(self, request, format=None):
        serializer = CustomerSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            token, created = Token.objects.get_or_create(user=serializer.instance)
            return Response({'Token': token.key}, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


def user_login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            user = authenticate(username=cd['email'], password=cd['password'])

            if user is not None:
                if user.is_active:
                    login(request, user)
                    return redirect(settings.LOGIN_REDIRECT_URL)
    else:
        form = LoginForm()
    return render(request, 'account/login.html', {'form': form})


class ObtainExpiringAuthToken(ObtainAuthToken):
    permission_classes = (AllowAny,)

    def post(self, request):
        password = request.data.get('password')
        email = request.data.get('email')
        customer = Customer.objects.get(email=email)
        username = customer.user.username
        data = {'username': username, 'password': password}
        serializer = self.serializer_class(data=data)
        if serializer.is_valid():
            token, created = Token.objects.get_or_create(user=serializer.validated_data['user'])

            if not created:
                # update the created time of the token to keep it valid
                token.created = datetime.datetime.utcnow()
                token.save()

            return Response({'token': token.key})
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


obtain_auth_token = ObtainAuthToken.as_view()
