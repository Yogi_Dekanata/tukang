"""klik_tukang URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
# from django.contrib import admin
# from apps.api.v1.users.views import home
# from rest_framework.authtoken.views import obtain_auth_token

from apps.users_authentication import views

urlpatterns = [
    # url(r'^admin/', include(admin.site.urls)),
    url(r'^admin/', include('apps.admin.delivery_man.urls', namespace='admin_delivery_man')),
    url(r'^admin/', include('apps.admin.customers.urls', namespace='admin_customers')),
    url(r'^admin/', include('apps.admin.dashboard.urls', namespace='admin_dashboard')),
    url(r'^admin/', include('apps.admin.users_management.urls', namespace='admin_users_management')),
    url(r'^admin/', include('apps.admin.trancation.urls', namespace='admin_trancation')),
    url(r'^api/', include('apps.api.v1.users.urls')),
    url(r'^api/', include('apps.api.v1.product_price.urls')),
    url(r'^api/', include('apps.api.v1.transaction.urls')),
    url(r'^api-token-auth/', views.ObtainExpiringAuthToken.as_view()),
    url(r'^login/', views.user_login, name='login'),
    url(r'^accounts/logout/', 'django.contrib.auth.views.logout', {'next_page': '/login'}, name='logout'),
    url(r'^registration-customer/', views.RegistrationCustomerView.as_view()),

]
#
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns